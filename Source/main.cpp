//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

void factorial (int userNum)
{
    int i, fact = 1;
    for (i = 1; i <= userNum; i++)
    {
        fact = fact * i;
    }
        
        std::cout << "integer value " << fact << std::endl;
}

int main (int argc, const char* argv[])
{

    // insert code here...
    int intNum = -1;
    while (intNum < 0) {
        std::cout << "Please enter a positive number\n";
        std::cin >> intNum;
    }
    factorial (intNum);
}

